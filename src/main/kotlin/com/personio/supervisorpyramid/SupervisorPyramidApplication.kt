package com.personio.supervisorpyramid

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SupervisorPyramidApplication

fun main(args: Array<String>) {
    runApplication<SupervisorPyramidApplication>(*args)
}
