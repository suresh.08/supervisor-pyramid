package com.personio.supervisorpyramid.domain

import javax.persistence.*

@Entity
@Table(name = "employee")
data class Employee(
        @Id
        val name: String = "",
        @ManyToOne(cascade=[CascadeType.ALL])
        @JoinColumn(name="supervisor")
        var supervisor: Employee? = null,
        @Transient
        var subordinates: List<Employee> = emptyList()
){
        override fun toString(): String {
                return "name=$name supervisor=${supervisor?.name} subordinates=$subordinates"
        }
}