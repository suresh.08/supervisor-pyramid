package com.personio.supervisorpyramid.domain

import com.personio.supervisorpyramid.api.validation.CheckCyclicRoots
import com.personio.supervisorpyramid.api.validation.CheckMultipleRoots
import java.util.*

data class EmployeeAggregator(val empToSupervisorReq: Map<String, String>,
                              val empToSubordinates: Map<String, MutableSet<String>>,
                              @CheckMultipleRoots(message = "{multiple.roots}") val roots: Set<String>,
                              @CheckCyclicRoots(message = "{cyclic.roots}") val cyclicRoots: Map<String, MutableSet<String>>,
                              val employeesInDescOrder: List<Employee>
){
    fun addManagerToEmployees(rootSupervisor: Employee?): List<Employee>{
        val prevSupervisor = Employee(employeesInDescOrder.first().name, rootSupervisor)
        val prevSupervisorsMap = mutableMapOf(employeesInDescOrder.first().name to prevSupervisor)
        val employeesWithManager = mutableListOf(prevSupervisor)
        for(i in 1 until employeesInDescOrder.size){
            val employee = employeesInDescOrder[i]
            val supervisor = prevSupervisorsMap[employee.supervisor!!.name]!!
            val employeeWithManager = employee.copy(supervisor = supervisor)
            employeesWithManager.add(employeeWithManager)
            prevSupervisorsMap[employeeWithManager.name] = employeeWithManager
        }
        return employeesWithManager
    }

    companion object {
        fun build(empToSupervisorReq: Map<String, String>): EmployeeAggregator{
            val empToSubordinates: Map<String, MutableSet<String>> = empToSubordinatesMap(empToSupervisorReq)
            val roots: Set<String> = roots(empToSupervisorReq)
            val cycRootNSortedEmployees= topologicalSort(empToSupervisorReq, empToSubordinates)
            val cyclicRoots: Map<String, MutableSet<String>> = cycRootNSortedEmployees.second.filter { it.value.isNotEmpty() }
            val employeesInOrder: List<Employee> = if(cycRootNSortedEmployees.first.isNotEmpty()) cycRootNSortedEmployees.first.reversed() else cycRootNSortedEmployees.first
            return EmployeeAggregator(empToSupervisorReq, empToSubordinates, roots, cyclicRoots, employeesInOrder)
        }

        private fun roots(empToSupervisorReq: Map<String, String>): Set<String> {
            val employeeSet = empToSupervisorReq.keys
            val supervisorSet = empToSupervisorReq.values.toSet()
            return supervisorSet - employeeSet
        }

        private fun empToSubordinatesMap(empToSupervisorReq: Map<String, String>): Map<String, MutableSet<String>> {
            val empToSubordinates = mutableMapOf<String, MutableSet<String>>()
            for (es in empToSupervisorReq) {
                if (!empToSubordinates.containsKey(es.key)) {
                    empToSubordinates[es.key] = mutableSetOf()
                }
                if (empToSubordinates.containsKey(es.value)) {
                    empToSubordinates[es.value]!!.add(es.key)
                } else {
                    empToSubordinates[es.value] = mutableSetOf(es.key)
                }
            }
            return empToSubordinates
        }

        private fun topologicalSort(empToSupervisorReq: Map<String, String>, empToSubordinates: Map<String, MutableSet<String>>):
                Pair<List<Employee>, Map<String, MutableSet<String>>> {
            val empToSubordinatesCloned = mutableMapOf<String, MutableSet<String>>()

            for(es in empToSubordinates){
                val clonedSet = mutableSetOf<String>()
                clonedSet.addAll(es.value)
                empToSubordinatesCloned[es.key] =  clonedSet
            }

            val queue: Queue<String> = LinkedList<String>()
            val tSorted = mutableListOf<Employee>()

            for (es in empToSubordinatesCloned) {
                if (es.value.isEmpty()) {
                    queue.offer(es.key)
                }
            }
            while (!queue.isEmpty()) {
                val emp = queue.remove()
                tSorted.add(Employee(emp, empToSupervisorReq[emp]?.let { Employee(it, null) }))
                if (empToSupervisorReq[emp] != null && empToSubordinatesCloned[empToSupervisorReq[emp]!!]!!.contains(emp)) {
                    empToSubordinatesCloned[empToSupervisorReq[emp]!!]!!.remove(emp)
                    if (empToSubordinatesCloned[empToSupervisorReq[emp]!!]!!.isEmpty()) {
                        queue.offer(empToSupervisorReq[emp]!!)
                    }
                }
            }

            return if (tSorted.size == empToSubordinatesCloned.size) {
                Pair(tSorted, emptyMap())
            } else {
                Pair(emptyList(), empToSubordinatesCloned)
            }
        }

    }
}