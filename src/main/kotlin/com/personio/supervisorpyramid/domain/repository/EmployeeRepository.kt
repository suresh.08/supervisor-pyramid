package com.personio.supervisorpyramid.domain.repository

import com.personio.supervisorpyramid.domain.Employee
import java.util.*

interface EmployeeRepository {

    fun findSubordinates(supervisor: String): List<EmployeeLevel>

    fun findSupervisors(employee: String): List<String>

    fun saveAll(employees: List<Employee>): Iterable<Employee>

    fun findById(name: String): Optional<Employee>

}