package com.personio.supervisorpyramid.domain.repository

import com.personio.supervisorpyramid.domain.User
import java.util.*

interface UserRepository {

    fun findByUsername(username: String): Optional<User>

    fun save(user: User): User

}