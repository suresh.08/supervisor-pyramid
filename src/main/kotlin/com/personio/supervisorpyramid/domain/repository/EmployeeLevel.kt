package com.personio.supervisorpyramid.domain.repository

data class EmployeeLevel(val employee: String, val level: Int)

fun List<String>.toEmployees(): List<EmployeeLevel> {
    return this.map {
        val parts = it.split(',')
        EmployeeLevel(parts[0], parts[1].toInt())
    }
}