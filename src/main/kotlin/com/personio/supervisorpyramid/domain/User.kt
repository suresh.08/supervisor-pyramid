package com.personio.supervisorpyramid.domain

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = "user")
data class User(
        @Id
        val username: String = "",
        val password: String = "",
        val role: String = ""
)