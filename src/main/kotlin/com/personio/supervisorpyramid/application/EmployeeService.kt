package com.personio.supervisorpyramid.application

import com.personio.supervisorpyramid.domain.EmployeeAggregator
import com.personio.supervisorpyramid.domain.repository.EmployeeLevel
import com.personio.supervisorpyramid.domain.repository.EmployeeRepository
import com.personio.supervisorpyramid.domain.repository.toEmployees
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service("employeeServiceImpl")
class EmployeeService{

    @Autowired
    private lateinit var  employeeRepository: EmployeeRepository

    fun addEmployees(employeeAggregator: EmployeeAggregator) {
        val maybeExistingRoot = employeeRepository.findById(employeeAggregator.employeesInDescOrder.first().name)
        val existingSupervisor = if (maybeExistingRoot.isPresent){
             maybeExistingRoot.get().supervisor
        }else null
        val employeesToCreate = employeeAggregator.addManagerToEmployees(existingSupervisor)
        employeeRepository.saveAll(employeesToCreate)
    }

    fun getSubordinates(name: String): LinkedHashMap<String, List<String>> {
        val maybeExistingRoot = employeeRepository.findById(name)
        if (!maybeExistingRoot.isPresent) return LinkedHashMap()
        val subordinates = employeeRepository.findSubordinates(name)
        return DfsMapBuilder.dfsMap(subordinates)
    }

    fun getSupervisors(name: String): Map<String, List<String>> {
        val employees = employeeRepository.findSupervisors(name).dropLast(1) // to remove the null manager from root
        return if(!employees.isEmpty()) mapOf(name to employees) else emptyMap()
    }
}