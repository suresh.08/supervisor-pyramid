package com.personio.supervisorpyramid.application

import com.personio.supervisorpyramid.domain.repository.EmployeeLevel


object DfsMapBuilder {

    fun dfsMap(employees: List<EmployeeLevel>): LinkedHashMap<String, List<String>> {
        if (employees.size == 1) return linkedMapOf(employees.first().employee to emptyList())
        val superVisorToEmployees = linkedMapOf<String, List<String>>()
        for (i in 0 until employees.size) {
            val e = employees[i]
            superVisorToEmployees[e.employee] = adjacenyList(i, employees, e.level)
        }
        return superVisorToEmployees
    }

    private fun adjacenyList(currIndex: Int, employees: List<EmployeeLevel>, currLevel: Int): List<String> {
        val adjacencyList = mutableListOf<String>()
        for (i in currIndex + 1 until employees.size) {
            val e = employees[i]
            if (e.level - 1 == currLevel) {
                adjacencyList.add(e.employee)
            } else if(e.level == currLevel) break
        }
        return adjacencyList
    }


}
