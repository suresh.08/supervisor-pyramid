package com.personio.supervisorpyramid.infrastructure.repository

import com.personio.supervisorpyramid.domain.User
import com.personio.supervisorpyramid.domain.repository.UserRepository
import org.springframework.stereotype.Component
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.security.crypto.password.PasswordEncoder
import javax.annotation.PostConstruct


@Component
@Profile("dev")
class UserCreator {
    @Autowired
    private lateinit var userRepository: UserRepository

    @Autowired
    private lateinit var passwordEncoder: PasswordEncoder

    @PostConstruct
    fun init() {
        val encryptedPassword = passwordEncoder.encode("C@Personio")
        userRepository.save(User("Chris", encryptedPassword, "admin"))
    }
}