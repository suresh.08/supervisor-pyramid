package com.personio.supervisorpyramid.infrastructure.repository.domain

import com.personio.supervisorpyramid.domain.Employee
import com.personio.supervisorpyramid.domain.repository.EmployeeLevel
import com.personio.supervisorpyramid.domain.repository.EmployeeRepository
import com.personio.supervisorpyramid.infrastructure.repository.sqlite.SQLiteEmployeeCrudRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import com.personio.supervisorpyramid.domain.repository.toEmployees

@Service("employeeRepositoryImpl")
class EmployeeRepositoryImpl: EmployeeRepository {

    @Autowired
    lateinit var employeeCrudRepository: SQLiteEmployeeCrudRepository

    override fun findById(name: String): Optional<Employee> {
        return employeeCrudRepository.findById(name)
    }

    override fun findSubordinates(supervisor: String): List<EmployeeLevel> {
        return employeeCrudRepository.findSubordinates(supervisor).toEmployees()
    }

    override fun findSupervisors(employee: String): List<String> {
        return employeeCrudRepository.findSupervisors(employee)
    }

    override fun saveAll(employees: List<Employee>): Iterable<Employee> {
        return employeeCrudRepository.saveAll(employees)
    }

}