package com.personio.supervisorpyramid.infrastructure.repository.sqlite

import com.personio.supervisorpyramid.domain.Employee
import org.springframework.context.annotation.Profile
import org.springframework.data.repository.CrudRepository
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Profile("dev")
interface SQLiteEmployeeCrudRepository : CrudRepository<Employee, String>{
    @Query("""WITH RECURSIVE
                       works_for_supervisor(name,level) AS
                       (VALUES(:supervisor, 0)
                               UNION
                       SELECT employee.name, works_for_supervisor.level+1 FROM employee JOIN works_for_supervisor ON employee.supervisor=works_for_supervisor.name
                       ORDER BY 2 DESC
                       ) SELECT * from works_for_supervisor;""",
            nativeQuery = true)
    fun findSubordinates(@Param("supervisor") supervisor: String): List<String>

    @Query("""WITH RECURSIVE
                       supervisor_of(name) AS
                       (SELECT supervisor FROM employee WHERE name=:employee
                               UNION ALL
                       SELECT supervisor FROM employee JOIN supervisor_of ON employee.name=supervisor_of.name)
                       SELECT * FROM supervisor_of;""",
            nativeQuery = true)
    fun findSupervisors(@Param("employee") employee: String): List<String>

}