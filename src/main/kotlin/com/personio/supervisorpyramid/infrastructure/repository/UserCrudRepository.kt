package com.personio.supervisorpyramid.infrastructure.repository

import com.personio.supervisorpyramid.domain.User
import org.springframework.context.annotation.Profile
import org.springframework.data.repository.CrudRepository
import java.util.*

@Profile("dev")
interface UserCrudRepository: CrudRepository<User, String>{
    fun findByUsername(username: String): Optional<User>
}