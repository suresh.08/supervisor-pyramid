package com.personio.supervisorpyramid.infrastructure.repository.domain

import com.personio.supervisorpyramid.domain.User
import com.personio.supervisorpyramid.domain.repository.UserRepository
import com.personio.supervisorpyramid.infrastructure.repository.UserCrudRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service("userRepositoryImpl")
class UserRepositoryImpl: UserRepository {

    @Autowired
    private lateinit var userCrudCrudRepository: UserCrudRepository

    override fun findByUsername(username: String): Optional<User> {
       return userCrudCrudRepository.findByUsername(username)
    }

    override fun save(user: User): User {
        return userCrudCrudRepository.save(user)
    }
}