package com.personio.supervisorpyramid.infrastructure


import com.personio.supervisorpyramid.domain.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Component

import java.util.Arrays

@Component
class DbUserDetailsService : UserDetailsService {
    @Autowired
    private lateinit var userRepository: UserRepository

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails {
        val user = userRepository.findByUsername(username)
        if(!user.isPresent) throw UsernameNotFoundException("User not found")
        val authorities = Arrays.asList(SimpleGrantedAuthority(user.get().role))
        return User(user.get().username, user.get().password, authorities)
    }
}