package com.personio.supervisorpyramid.api.dto

import com.personio.supervisorpyramid.domain.Employee

object NestedResponseBuilder {

    fun toNestedTree(map: java.util.LinkedHashMap<String, List<String>>, root: String): MutableMap<String, Any> {
        return generateMap(root, map[root]!!, mutableMapOf(root to mutableMapOf<String, Any>()), map)
    }

    fun generateMap(emp: String, subordinates: List<String>, result: MutableMap<String, Any>, map: java.util.LinkedHashMap<String, List<String>>): MutableMap<String, Any> {
        if (subordinates.isEmpty()) {
            return result
        }
        for (e in subordinates) {
            if (result.containsKey(emp)) {
                val existingMap = result[emp]!! as MutableMap<String, Any>
                result[emp] = existingMap.plus(generateMap(e, map[e]!!, mutableMapOf(e to mutableMapOf<String, Any>()), map))
            } else {
                result[emp] = generateMap(e, map[e]!!, mutableMapOf(e to mutableMapOf<String, Any>()), map)
            }
        }
        return result
    }

    fun getResponse(empToSubordinates: Map<String, MutableSet<String>>,
                    employeesInOrder: List<Employee>,
                    root: String): MutableMap<String, Any> {
        val orderedMap = setupOrderedEmpToSubordinates(empToSubordinates, employeesInOrder)
        return generateMap(root, orderedMap[root]!!, mutableMapOf(root to mutableMapOf<String, Any>()), orderedMap)
    }

    private fun setupOrderedEmpToSubordinates(empToSubordinates: Map<String, MutableSet<String>>,
                                              employeesInOrder: List<Employee>): java.util.LinkedHashMap<String, List<String>> {
        val orderedEtoS = linkedMapOf<String, List<String>>()
        for (emp in employeesInOrder) {
            orderedEtoS[emp.name] = empToSubordinates[emp.name]?.let { it.toList() } ?: emptyList<String>()
        }
        return orderedEtoS
    }

}