package com.personio.supervisorpyramid.api.resource

import com.personio.supervisorpyramid.api.dto.NestedResponseBuilder
import com.personio.supervisorpyramid.api.exception.ResourceNotFoundException
import com.personio.supervisorpyramid.api.validation.EntityValidator
import com.personio.supervisorpyramid.application.EmployeeService
import com.personio.supervisorpyramid.domain.EmployeeAggregator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import org.springframework.validation.Validator
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid


@Validated
@RestController
@RequestMapping("employee")
class EmployeeSupervisorResource {

    @Autowired
    private lateinit var entityValidator: EntityValidator

    @Autowired
    private lateinit var validator: Validator

    @Autowired
    private lateinit var employeeService: EmployeeService

    @PostMapping("/employee_supervisor")
    @ResponseStatus(HttpStatus.CREATED)
    fun addEmployeesWithSupervisor(@RequestBody @Valid employeesToSuperVisor: Map<String, String>, bindingResult: BindingResult, request: HttpServletRequest): ResponseEntity<Any> {
        val rootAggregate = EmployeeAggregator.build(employeesToSuperVisor)
        validator.validate(rootAggregate, bindingResult)
        if (bindingResult.hasErrors()) {
            return entityValidator.buildError(request, bindingResult)
        }
        employeeService.addEmployees(rootAggregate)

        val response = NestedResponseBuilder.getResponse(rootAggregate.empToSubordinates,
                rootAggregate.employeesInDescOrder,
                rootAggregate.employeesInDescOrder.first().name)
        val headers = HttpHeaders()
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8.toString())
        return ResponseEntity.status(HttpStatus.CREATED).headers(headers).body(response)
    }

    @GetMapping("/{name}/subordinate_tree")
    fun getSubordinates(@PathVariable name: String): ResponseEntity<Map<String, Any>> {
        val headers = HttpHeaders()
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8.toString())
        val tree = employeeService.getSubordinates(name)
        if (tree.isEmpty()) throw ResourceNotFoundException("Employee $name not found")
        val response = NestedResponseBuilder.toNestedTree(tree, name)
        return ResponseEntity.ok().headers(headers).body(response)
    }

    @GetMapping("/{name}/supervisors")
    fun getSupervisors(@PathVariable name: String): ResponseEntity<Map<String, List<String>>> {
        val headers = HttpHeaders()
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8.toString())
        val employees = employeeService.getSupervisors(name)
        if (employees.isEmpty()) throw ResourceNotFoundException("Employee $name not found")
        return ResponseEntity.ok().headers(headers).body(employees)
    }
}