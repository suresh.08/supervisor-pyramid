package com.personio.supervisorpyramid.api.exception

import java.util.*

data class ApiError(val field: String, val reason: String, val rejectedValue: String)

data class ErrorDetails(val path: String, val errors: List<ApiError>, val date: Date = Date())