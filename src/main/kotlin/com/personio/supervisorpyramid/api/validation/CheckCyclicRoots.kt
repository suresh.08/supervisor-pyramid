package com.personio.supervisorpyramid.api.validation

import javax.validation.Constraint
import javax.validation.Payload
import javax.validation.ReportAsSingleViolation
import kotlin.reflect.KClass

@MustBeDocumented
@Constraint(validatedBy = [CyclicRootValidator::class])
@Target(
        AnnotationTarget.FUNCTION, AnnotationTarget.FIELD, AnnotationTarget.ANNOTATION_CLASS,
        AnnotationTarget.PROPERTY_GETTER
)
@Retention(AnnotationRetention.RUNTIME)
@ReportAsSingleViolation
annotation class CheckCyclicRoots (
        val message: String = "invalid root",
        val groups: Array<KClass<*>> = [],
        val payload: Array<KClass<out Payload>> = []
)