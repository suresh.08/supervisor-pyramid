package com.personio.supervisorpyramid.api.validation

import com.personio.supervisorpyramid.api.exception.ApiError
import com.personio.supervisorpyramid.api.exception.ErrorDetails
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.validation.BindingResult
import javax.servlet.http.HttpServletRequest

@Service("entityValidator")
class EntityValidator {

    fun buildError(request: HttpServletRequest, bindingResult: BindingResult): ResponseEntity<Any> {
        val apisErrors = bindingResult.fieldErrors.map {
            ApiError(it.field, it.defaultMessage ?: "", it.rejectedValue.toString())
        }
        return ResponseEntity(ErrorDetails(request.requestURI, apisErrors), HttpStatus.UNPROCESSABLE_ENTITY)
    }

}