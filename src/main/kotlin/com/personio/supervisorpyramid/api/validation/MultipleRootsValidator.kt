package com.personio.supervisorpyramid.api.validation

import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

class MultipleRootsValidator: ConstraintValidator<CheckMultipleRoots, Set<String>> {

    private var message : String? = null

    override fun isValid(value: Set<String>, context: ConstraintValidatorContext?): Boolean {
        if (value.size > 1){
            //disable existing violation message
            context?.disableDefaultConstraintViolation()
            //build new violation message and add it
            context?.buildConstraintViolationWithTemplate("$message $value")?.addConstraintViolation()
        }
        return value.size <= 1
    }

    override fun initialize(constraintAnnotation: CheckMultipleRoots?) {
        message = constraintAnnotation?.message
    }
}