package com.personio.supervisorpyramid.api.validation

import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext

class CyclicRootValidator: ConstraintValidator<CheckCyclicRoots, Map<String, MutableSet<String>>> {

    private var message : String? = null

    override fun isValid(value: Map<String, MutableSet<String>>, context: ConstraintValidatorContext?): Boolean {
        if (value.isNotEmpty()){
            //disable existing violation message
            context?.disableDefaultConstraintViolation()
            //build new violation message and add it
            val cyclicPath = value.filterValues { it.isNotEmpty() }.toString().replace("=[", " is supervisor of ").replace("]", "")
            context?.buildConstraintViolationWithTemplate(message + cyclicPath)?.addConstraintViolation()
        }
        return value.isEmpty()
    }

    override fun initialize(constraintAnnotation: CheckCyclicRoots?) {
        message = constraintAnnotation?.message
    }
}