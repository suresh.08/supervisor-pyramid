package com.personio.supervisorpyramid.application

import com.personio.supervisorpyramid.domain.repository.EmployeeLevel
import com.personio.supervisorpyramid.domain.repository.toEmployees
import org.junit.Assert.*
import org.junit.Test

class EmployeeLevelTest{
    @Test
    fun `can build  response map correctly`() {
        val subordinates = listOf(
                "Sophie,0",
                "Nick,1",
                "Barbara,2",
                "Pete,2"
        )
        val result = subordinates.toEmployees()
        assertEquals(result.size, 4)
        assertEquals(EmployeeLevel("Sophie", 0), result.first())
    }
}