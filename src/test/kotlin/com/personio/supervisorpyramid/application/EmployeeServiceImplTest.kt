package com.personio.supervisorpyramid.application

import com.personio.supervisorpyramid.domain.Employee
import com.personio.supervisorpyramid.domain.repository.EmployeeLevel
import com.personio.supervisorpyramid.domain.repository.EmployeeRepository
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.collection.IsMapContaining
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyString
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class EmployeeServiceImplTest {

    @Mock
    private lateinit var employeeRepository: EmployeeRepository

    @InjectMocks
    private lateinit var employeeServiceImpl: EmployeeService

    @Test
    fun `get supervisors removing last elem`() {
        Mockito.`when`(employeeRepository.findSupervisors("S")).thenReturn(listOf("S", "J", "null"))
        val result = employeeServiceImpl.getSupervisors("S")
        assertThat(result, IsMapContaining.hasKey("S"))
        assertThat(result, IsMapContaining.hasValue(listOf("S", "J")))
    }

    @Test
    fun `get subordinates `() { //"J,0", "S,1", "N,2"
        Mockito.`when`(employeeRepository.findSubordinates(anyString())).thenReturn(listOf(
                EmployeeLevel("J", 0),
                EmployeeLevel("S", 1),
                EmployeeLevel("N", 2)))
        Mockito.`when`(employeeRepository.findById(anyString())).thenReturn(Optional.of(Employee("S")))
        val result = employeeServiceImpl.getSubordinates("S")
        assertEquals(3, result.size)
    }

    @Test
    fun `get subordinates returns empty map for employee not found in db`() {
        Mockito.`when`(employeeRepository.findById(anyString())).thenReturn(Optional.empty())
        val result = employeeServiceImpl.getSubordinates("S")
        assert(result.isEmpty())
    }


}