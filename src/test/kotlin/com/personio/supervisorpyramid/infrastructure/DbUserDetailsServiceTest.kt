package com.personio.supervisorpyramid.infrastructure

import com.personio.supervisorpyramid.domain.User
import com.personio.supervisorpyramid.domain.repository.UserRepository
import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class DbUserDetailsServiceTest {

    @Mock
    private lateinit var userRepository: UserRepository

    @InjectMocks
    private lateinit var dbUserDetailsService: DbUserDetailsService

    @Test
    fun loadUserByUsername() {
        Mockito.`when`(userRepository.findByUsername("user")).thenReturn(Optional.of(User("user", "password", "admin")))
        val userDetails = dbUserDetailsService.loadUserByUsername("user")
        assertEquals("user", userDetails.username)
        assertEquals("password", userDetails.password)
        assertEquals("admin", userDetails.authorities.first().authority)
    }
}