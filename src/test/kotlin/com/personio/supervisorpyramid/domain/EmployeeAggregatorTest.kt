package com.personio.supervisorpyramid.domain

import org.junit.Assert.*
import org.junit.Test

class EmployeeAggregatorTest{
    @Test
    fun `root is extracted correctly`(){
        val empToSupervisor = mapOf("Barbara" to "Nick",
                "Pete" to "Nick",
                "Nick" to "Sophie",
                "Sophie" to "Jonas"
        )
        val roots = EmployeeAggregator.build(empToSupervisor).roots
        assertEquals(roots.size, 1)
        assertEquals(roots.first(), "Jonas")

        val cyclicRoots =  EmployeeAggregator.build(empToSupervisor).cyclicRoots
        assertEquals(cyclicRoots.isEmpty(), true)

        val root =  EmployeeAggregator.build(empToSupervisor).employeesInDescOrder.first().name
        assertEquals(root, "Jonas")
    }

    @Test
    fun `can return multiple roots for invalid request`(){
        val empToSupervisor = mapOf("Barbara" to "Nick",
                "Pete" to "Nick",
                "Nick" to "Sophie",
                "Sophie" to "Jonas",
                "Arya" to "Jon"
        )
        val roots = EmployeeAggregator.build(empToSupervisor).roots
        assertEquals(roots.size, 2)
        assertEquals(roots, setOf("Jonas", "Jon"))

        val cyclicRoots =  EmployeeAggregator.build(empToSupervisor).cyclicRoots
        assertEquals(cyclicRoots.isEmpty(), true)
    }

    @Test
    fun `can return no root for invalid request`(){
        val empToSupervisor = mapOf("Barbara" to "Nick",
                "Pete" to "Nick",
                "Nick" to "Sophie",
                "Sophie" to "Jonas",
                "Jonas" to "Pete"
        )
        val roots = EmployeeAggregator.build(empToSupervisor).roots
        assertEquals(roots.isEmpty(), true)

        val cyclicRoots =  EmployeeAggregator.build(empToSupervisor).cyclicRoots
        assertEquals( 4, cyclicRoots.size)
    }


    @Test
    fun `cyclic root returns empty for valid relation`(){
        val empToSupervisor = mapOf("Barbara" to "Nick",
                "Pete" to "Nick",
                "Nick" to "Sophie",
                "Sophie" to "Jonas"
        )

        val root = EmployeeAggregator.build(empToSupervisor).employeesInDescOrder.first().name
        assertEquals(root, "Jonas")

        val cyclicRoots =  EmployeeAggregator.build(empToSupervisor).cyclicRoots
        assertEquals(cyclicRoots.isEmpty(), true)
    }

    @Test
    fun `cyclic root returns employees with looped relation`(){
        val empToSupervisor = mapOf("Barbara" to "Nick",
                "Pete" to "Nick",
                "Nick" to "Sophie",
                "Sophie" to "Barbara"
        )

        val root = EmployeeAggregator.build(empToSupervisor).employeesInDescOrder
        assertTrue(root.isEmpty())

        val cyclicRoots =  EmployeeAggregator.build(empToSupervisor).cyclicRoots
        assertTrue(cyclicRoots.isNotEmpty())
    }

    @Test
    fun `return null root if there is one root but has cyclic relation`(){
        val empToSupervisor = mapOf("Barbara" to "Nick",
                "Pete" to "Nick",
                "Nick" to "Sophie",
                "Sophie" to "Barbara",
                "abc" to "xyzs"
        )

        val root = EmployeeAggregator.build(empToSupervisor).employeesInDescOrder
        assertTrue(root.isEmpty())

        val roots = EmployeeAggregator.build(empToSupervisor).roots
        assertEquals(roots.size, 1)

        val cyclicRoots =  EmployeeAggregator.build(empToSupervisor).cyclicRoots
        assertTrue(cyclicRoots.isNotEmpty())
    }
}