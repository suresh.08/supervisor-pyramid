package com.personio.supervisorpyramid

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.context.annotation.PropertySource
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner

@PropertySource(value = ["classpath:*.properties"])
@RunWith(SpringRunner::class)
@ContextConfiguration(initializers = [ConfigFileApplicationContextInitializer::class])
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [SupervisorPyramidApplication::class])
class SupervisorPyramidApplicationTests {

    @LocalServerPort
    private val port: Int = 0
    var restTemplate = TestRestTemplate()
    var headers = HttpHeaders()

    @Test
    fun `can create employees with correct hierarchy`() {
        val req = mapOf("Barbara" to "Nick",
                "Pete" to "Nick",
                "Nick" to "Sophie",
                "Sophie" to "Jonas"
        )
        val entity = HttpEntity(req, headers)
        val response = restTemplate.withBasicAuth("Chris", "C@Personio").exchange(
                createURLWithPort("/employee/employee_supervisor"), HttpMethod.POST, entity, object : ParameterizedTypeReference<Map<String, Any>>() {})
        val statusCode = response.statusCode
        assertEquals(HttpStatus.CREATED, statusCode)
        assertEquals("{Jonas={Sophie={Nick={Barbara={}, Pete={}}}}}", response.body.toString())

        val updateReq = mapOf("Nick" to "Jonas")
        val updateEntity = HttpEntity(updateReq, headers)
        val updateResponse = restTemplate
                .withBasicAuth("Chris", "C@Personio")
                .exchange(createURLWithPort("/employee/employee_supervisor"),
                        HttpMethod.POST,
                        updateEntity,
                        object : ParameterizedTypeReference<Map<String, Any>>() {})
        val updateStatusCode = response.statusCode
        assertEquals(HttpStatus.CREATED, updateStatusCode)
        assertEquals("{Jonas={Nick={}}}", updateResponse.body.toString())

        val qrySupervisorEntityEntity = HttpEntity("parameters", HttpHeaders())
        val qrySupervisorEntityResponse = restTemplate
                .withBasicAuth("Chris", "C@Personio")
                .exchange(createURLWithPort("/employee/Nick/subordinate_tree"),
                        HttpMethod.GET, qrySupervisorEntityEntity, object : ParameterizedTypeReference<Map<String, Any>>() {})
        val qrySupervisorEntityStatusCode = response.statusCode
        assertEquals(HttpStatus.CREATED, qrySupervisorEntityStatusCode)
        assertEquals("{Nick={Barbara={}, Pete={}}}", qrySupervisorEntityResponse.body.toString())

        val qrySubordinateEntity = HttpEntity("parameters", HttpHeaders())
        val qrySubordinateResponse = restTemplate
                .withBasicAuth("Chris", "C@Personio")
                .exchange(createURLWithPort("/employee/Pete/supervisors"),
                        HttpMethod.GET, qrySubordinateEntity, object : ParameterizedTypeReference<Map<String, Any>>() {})
        val qrySubordinateStatusCode = qrySubordinateResponse.statusCode
        assertEquals(HttpStatus.OK, qrySubordinateStatusCode)
        assertEquals(mapOf("Pete" to listOf("Nick", "Jonas")), qrySubordinateResponse.body)
    }

    @Test
    fun `can create employees with correct hierarchy for nested leaf nodes`() {
        val req = mapOf("Bob" to "Alice",
                "Cindy" to "Alice",
                "Dave" to "Bob",
                "Emma" to "Bob",
                "Peter" to "Emma",
                "Fred" to "Cindy",
                "Gail" to "Cindy"
        )
        val entity = HttpEntity(req, headers)
        val response = restTemplate.withBasicAuth("Chris", "C@Personio").exchange(
                createURLWithPort("/employee/employee_supervisor"), HttpMethod.POST, entity, object : ParameterizedTypeReference<Map<String, Any>>() {})
        val statusCode = response.statusCode
        assertEquals(HttpStatus.CREATED, statusCode)
        assertEquals("{Alice={Bob={Dave={}, Emma={Peter={}}}, Cindy={Fred={}, Gail={}}}}", response.body.toString())
    }

    @Test
    fun `throws 401 for unathorized req`() {
        val qrySubordinateEntity = HttpEntity("parameters", HttpHeaders())
        val qrySubordinateResponse = restTemplate
                .exchange(createURLWithPort("/employee/Pete/supervisors"),
                        HttpMethod.GET, qrySubordinateEntity, object : ParameterizedTypeReference<String>() {})
        val qrySubordinateStatusCode = qrySubordinateResponse.statusCode
        assertEquals(HttpStatus.UNAUTHORIZED, qrySubordinateStatusCode)
    }

    @Test
    fun `throws 404 for resource not found`() {
        val qrySubordinateEntity = HttpEntity("parameters", HttpHeaders())
        val qrySubordinateResponse = restTemplate
                .withBasicAuth("Chris", "C@Personio")
                .exchange(createURLWithPort("/employee/xyz/supervisors"),
                        HttpMethod.GET, qrySubordinateEntity, object : ParameterizedTypeReference<String>() {})
        val qrySubordinateStatusCode = qrySubordinateResponse.statusCode
        assertEquals(HttpStatus.NOT_FOUND, qrySubordinateStatusCode)
    }

    @Test
    fun `throws bad request for multiple roots`() {
        val req = mapOf("Barbara" to "Nick",
                "Pete" to "Nick",
                "Nick" to "Sophie",
                "Sophie" to "Jonas",
                "abc" to "xyzå"
        )
        val entity = HttpEntity(req, headers)
        val response = restTemplate.withBasicAuth("Chris", "C@Personio").exchange(
                createURLWithPort("/employee/employee_supervisor"), HttpMethod.POST, entity, object : ParameterizedTypeReference<String>() {})
        val statusCode = response.statusCode
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, statusCode)

    }

    @Test
    fun `throws bad request for cyclic roots`() {
        val req = mapOf("Barbara" to "Nick",
                "Pete" to "Nick",
                "Nick" to "Sophie",
                "Sophie" to "Jonas",
                "Jonas" to "Pete"
        )
        val entity = HttpEntity(req, headers)
        val response = restTemplate.withBasicAuth("Chris", "C@Personio").exchange(
                createURLWithPort("/employee/employee_supervisor"), HttpMethod.POST, entity, object : ParameterizedTypeReference<String>() {})
        val statusCode = response.statusCode
        assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, statusCode)

    }

    private fun createURLWithPort(uri: String): String {
        return "http://localhost:$port$uri"
    }
}
