package com.personio.supervisorpyramid.api.dto

import com.personio.supervisorpyramid.application.DfsMapBuilder
import com.personio.supervisorpyramid.domain.repository.EmployeeLevel
import org.junit.Assert.assertEquals
import org.junit.Test

class DfsMapBuilderTest {

    @Test
    fun `can traverse dfs and create map`() {
        val elems = listOf(
                EmployeeLevel("Jonas", 0),
                EmployeeLevel("Sophie", 1),
                EmployeeLevel("Barbara", 2),
                EmployeeLevel("Nick", 2),
                EmployeeLevel("Pete", 3)
        )

        val result = DfsMapBuilder.dfsMap(elems)
        val expected = mapOf("Jonas" to listOf("Sophie"),
                "Sophie" to listOf("Barbara", "Nick"),
                "Barbara" to emptyList(),
                "Nick" to listOf("Pete"),
                "Pete" to emptyList())
        assertEquals(expected, result)

    }


    @Test
    fun `can traverse dfs and create map with multiple top level employees`() {
        val elems = listOf(
                EmployeeLevel("Jonas", 0),
                EmployeeLevel("Sophie", 1),
                EmployeeLevel("Barbara", 2),
                EmployeeLevel("Nick", 2),
                EmployeeLevel("Sophie2", 1),
                EmployeeLevel("Barbara2", 2),
                EmployeeLevel("Pete", 3)
        )

        val result = DfsMapBuilder.dfsMap(elems)
        val expected = mapOf("Jonas" to listOf("Sophie", "Sophie2"),
                "Sophie" to listOf("Barbara", "Nick"),
                "Barbara" to emptyList(),
                "Nick" to emptyList(),
                "Sophie2" to listOf("Barbara2"),
                "Barbara2" to listOf("Pete"),
                "Pete" to emptyList())

        assertEquals(expected, result)
    }

    @Test
    fun `can return valid map for single entry`(){
    val elems = listOf(
            EmployeeLevel("Jonas", 0))
    val result = DfsMapBuilder.dfsMap(elems)
        assertEquals(mapOf("Jonas" to emptyList<String>()), result)
    }

    @Test
    fun `can handle empty input collection`(){
        val elems = emptyList<EmployeeLevel>()
        val result = DfsMapBuilder.dfsMap(elems)
        assertEquals(emptyMap<String, List<String>>(), result)
    }
}