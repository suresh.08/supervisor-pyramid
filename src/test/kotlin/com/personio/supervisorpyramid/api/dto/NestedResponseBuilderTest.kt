package com.personio.supervisorpyramid.api.dto

import org.junit.Assert.assertEquals
import org.junit.Test

class NestedResponseBuilderTest {

    @Test
    fun generateMap() {
        val map = linkedMapOf("J" to listOf("N", "S"), "N" to listOf("B", "P"), "P" to listOf("A"),"A" to emptyList(), "B" to emptyList() , "S" to emptyList())
        val result = NestedResponseBuilder.generateMap("J", listOf("N", "S"), mutableMapOf("J" to mutableMapOf<String, Any>()), map)
        assertEquals("{J={N={B={}, P={A={}}}, S={}}}", result.toString())
    }
}