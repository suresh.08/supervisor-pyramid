# supervisor-pyramid
As the name suggests, this project exposes REST api with Json payload to maintain employee-supervisor relationship.

# Application design 
App structure is influenced by DDD principles and consist of 4 modules:
1. `Domain` - core module and encapsulates all business logic and domain entities
2. `Application` - A thin layer depends on domain and works as glue between api and infra layer. It does not depend upon infra layer and domain repositories are injected using DI framework.
3. `Api` - Includes endpoint, validation, req, res dtos and it depends upon domain and application
4. `Infrastructure` - Communicates with external dependencies such as db, authentication, config, swagger documentation etc.. This layer is also dependent upon domain layer.

# Endpoints:
1. POST /employee/employee_supervisor -- create employees with supervisor 
2. GET /employee/{name}/subordinate_tree -- get an employee's all subordinates
3. GET /employee/{name}/supervisors -- get an employee's all supervisor

# Api swagger docs can be found at - 
http://localhost:8080/swagger-ui.html#!/clover-restaurant-sync-controller/createRestaurantUsingPOST

# Environment
- App is configured to work in ``dev`` environment. 
- ``dev`` env used ``SQLite in memory database`` and is configured using ``application-dev.properties``.
- ``dev`` env inserts user data on startup for authentication.
- To configure ``prod env`` modify ``spring.profiles.active`` property to ``prod`` and add ``application-prod.properties``.

# Authentication
 Login pages or session identification are good for web based clients involving human interaction. Also, it makes it to keep the api's stateless. 
 Basic Authentication or OAUTH token provides a solution for this problem as each request is independent of other request and server does not maintain any state information for the client, which is good for scalability point of view. For simplicity, I have used Basic Authentication.
 
 On startup it load up user information with credentials as - 
 username - ```Chris```
 password - ```C@Personio```
 
# Performance in IO operations
 1. Used recursive query to get employee hierarchy in both the directions, which optimises the no of calls and payload to db. 
 2. Used batch insert for create/update employees with supervisors. Batch size can be configured using the property ```spring.jpa.properties.hibernate.jdbc.batch_size```
 
# Test Coverage 
 More than `95%` with integration and unit tests 
 
# To run the app
 - unit tests ```sudo ./gradlew cleanTest test``` (to run tests every time)
 - build jar ```sudo ./gradlew bootJar```
 - run the jar ``` java -jar build/libs/supervisor-pyramid-0.0.1-SNAPSHOT.jar``` (make sure port 8080 is available, or change the the port using property ```server.port```)

# Run with dockerfile
- ```docker build -t supervisor-pyramid .```
- ```docker run -p 8080:8080 supervisor-pyramid:latest```
